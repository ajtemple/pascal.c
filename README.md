# Pascal.c

Decided to have a go at a friend's uni assignment to get back into C.

Prints however many lines of Pascal's Triangle as you want, up to 20. Upside down because that's what the assignment specified.

Compiling: `gcc ./pascal.c -o pascal`

Syntax: `./pascal <rows>`
